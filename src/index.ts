import * as Koa from 'koa';
import * as cors from '@koa/cors';
import * as helmet from 'koa-helmet';
import * as logger from 'koa-logger';
import * as bodyParser from 'koa-bodyparser';
import * as mongoose from 'mongoose';
import * as jwt from 'koa-jwt';
import { router } from './routes/routes';

mongoose.connect('mongodb://localhost:27017/koa', {useNewUrlParser: true, useFindAndModify: false});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

const app: any = new Koa();
const port = 8000;
app.use(cors()); // cross-origin resource sharing
app.use(helmet()); // safety headers
app.use(logger()); // server logger
app.use(bodyParser()); // parse body to context.request.body

// verify jwt token to all routes not starting with /public
app.use(jwt({ secret: 'my-super-secure-secret' }).unless({ path: [/^\/public/] }));
app.use(router.routes()); // register router middleware

app.listen(port); // run server
console.log(`Server listening on http://localhost:${port}`);
