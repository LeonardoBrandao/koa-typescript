import * as r from 'koa-router';
import { UserController } from '../controllers/user.controller';
import { LoginController } from '../controllers/login.controller';

const router = new r();

router
  // Healthcheck
  .get('/public/healthcheck', (ctx, next) => {
    ctx.body = { status: 'ok' }
    next()
  })

  // Login routes
  .post('/public/login', LoginController.login)

  // Users routes
  .get('/api/users', UserController.getAllUsers)
  .post('/api/users', UserController.addUser)

  // User routes
  .get('/api/user/:id', UserController.getUser)
  .put('/api/user/:id', UserController.updateUser)
  .delete('/api/user/:id', UserController.deleteUser);

export { router };
