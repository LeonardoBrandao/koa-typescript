import * as mongoose from 'mongoose';
import { UserSchema } from '../schemas/user.schema';

export default mongoose.model('users', UserSchema);
