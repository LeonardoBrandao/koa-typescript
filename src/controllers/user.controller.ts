import User from '../models/user.model';

export class UserController {
    static async getAllUsers(ctx, next) {
        const users = await User.find().exec();
        ctx.body = users;
        next();
    }

    static async getUser(ctx, next) {
        const { id } = ctx.params;
        const user = await User.findById(id).exec();
        ctx.body = user;
        next();
    }

    static async addUser(ctx, next) {
        const { body } = ctx.request;
        const user = new User(body);
        const response = await user.save();
        ctx.body = response;
        next();
    }

    static async updateUser(ctx, next) {
        const { id } = ctx.params;
        const { body } = ctx.request;
        const updatedUser = await User.findByIdAndUpdate(id, body);
        ctx.body = updatedUser;
        next();
    }

    static async deleteUser(ctx, next) {
        const { id } = ctx.params;
        const deletedUser = await User.deleteOne({ _id: id });
        ctx.body = deletedUser;
        next();
    }

}
