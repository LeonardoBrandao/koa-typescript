import * as jwt from 'jsonwebtoken';
import User from '../models/user.model';

export class LoginController {
    static async login(ctx, next) {
        const { id } = ctx.request.body; // same as const id = ctx.request.body.id
        const user = await LoginController.getUser(id);

        // if user was found, return jwt token, else, throw error
        if(user) {
            const token = jwt.sign({
                data: user,
              }, 'my-super-secure-secret', { expiresIn: '1h' });
            ctx.body = { token };
            next();
        } else {
            ctx.throw(404, 'User not found');
        }
    }

    private static async getUser(id) {
        try {
            const user = await User.findById(id).exec();
            return user;
        } catch {
            return false;
        }
    }
}
